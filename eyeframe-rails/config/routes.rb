Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admins', as: 'rails_admin'
  devise_for :admin_user

  resources :devices, only: [:create], param: :uuid do
    post :create_pin, on: :member
    post :pair_devices, on: :member
  end
end
