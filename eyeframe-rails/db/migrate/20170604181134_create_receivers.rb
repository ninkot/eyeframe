class CreateReceivers < ActiveRecord::Migration[5.1]
  def change
    create_table :receivers do |t|
      t.string :uuid
      t.string :pin
      t.integer :provider_id
      
      t.timestamps
    end
  end
end
