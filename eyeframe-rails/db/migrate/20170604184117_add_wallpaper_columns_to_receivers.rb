class AddWallpaperColumnsToReceivers < ActiveRecord::Migration[5.1]
  def up
    add_attachment :receivers, :wallpaper
  end

  def down
    remove_attachment :receivers, :wallpaper
  end
end
