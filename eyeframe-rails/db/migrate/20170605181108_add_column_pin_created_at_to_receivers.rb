class AddColumnPinCreatedAtToReceivers < ActiveRecord::Migration[5.1]
  def change
    add_column :receivers, :pin_created_at, :datetime
  end
end
