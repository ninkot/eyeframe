class DevicesController < ApplicationController
  def create
    @provider = Provider.new(device_params)
    @receiver = Receiver.new(device_params)
    render json: register_status
  end

  def create_pin
    @receiver = Receiver.find_by_uuid(params[:uuid])
    @receiver.pin = params[:pin]
    @receiver.pin_created_at = Time.zone.now
    render json: pin_status
  end

  def pair_devices
    @provider = Provider.find_by_uuid(params[:uuid])
    @receiver = Receiver.find_by_pin(params[:pin])
    unless @receiver
      render json: { status: "error", message: t_scoped(:error) }
    return
    end
    render json: pair_status
  end

  private

  def pair_status
    if (Time.zone.now - @receiver.pin_created_at) <= 10.minutes
      @provider.receivers << @receiver
      @receiver.remove_pin
      { status: "success", message: t_scoped(:success) }
    else
      { status: "error", message: t_scoped(:timeout) }
    end
  end

  def pin_status
    if @receiver.save
      { status: "success", message: t_scoped(:success) }
    else
      { status: "error", message: full_errors(@receiver) }
    end
  end

  def register_status
    if @provider.save && @receiver.save
      { status: "success", message: t_scoped(:success) }
    else
      { status: "error", message: "Provider's #{full_errors(@provider)}" +
                                  " and Receiver's #{full_errors(@receiver) unless @receiver.save}" }
    end
  end

  def full_errors(subject)
    subject.errors.full_messages.to_sentence
  end

  def device_params
    params.permit(:uuid, :pin)
  end
end
