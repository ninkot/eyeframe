class Provider < ApplicationRecord
  has_many :receivers

  validates :uuid, presence: true, uniqueness: true
end
