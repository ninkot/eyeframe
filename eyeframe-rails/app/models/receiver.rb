class Receiver < ApplicationRecord
  belongs_to :provider, optional: true

  validates :uuid, presence: true, uniqueness: true

  has_attached_file :wallpaper
  validates_attachment_content_type :wallpaper, content_type: /\Aimage\/.*\z/

  def remove_pin
    self.pin = nil
    self.pin_created_at = nil
    save
  end
end
