package com.bilskim.eyeframe;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by bilskim on 22/06/2017.
 */

class JSONAdapter extends BaseAdapter implements ListAdapter {
    private final Activity activity;
    private final JSONArray jsonArray;
    JSONAdapter(Activity activity, JSONArray jsonArray) {
        assert activity != null;
        assert jsonArray != null;

        this.jsonArray = jsonArray;
        this.activity = activity;
    }

    @Override public int getCount() {
        return jsonArray.length();
    }

    @Override public JSONObject getItem(int position) {
        return jsonArray.optJSONObject(position);
    }

    @Override public long getItemId(int position) {
        JSONObject jsonObject = getItem(position);

        return jsonObject.optLong("id");
    }

    @Override public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = activity.getLayoutInflater().inflate(R.layout.row, null);

        JSONObject jsonObject = getItem(position);
        TextView tv = (TextView)convertView.findViewById(R.id.uuid);
        try {
            tv.setText(jsonObject.getString("uuid"));
        } catch (JSONException e) {
        }

        return convertView;
    }
}