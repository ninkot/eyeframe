package com.bilskim.eyeframe;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

public class MainActivity extends AppCompatActivity {
    public static final String URL = "http://192.168.1.4:3000";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkRegistrationUUID();
        setReceiverOnClickListener();
        setProviderOnClickListener();
    }

    private void setProviderOnClickListener() {
        Button providerBtn = (Button)findViewById(R.id.provider_btn);
        providerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent providerIntent = new Intent(MainActivity.this, ProviderActivity.class);
                startActivity(providerIntent);
            }
        });
    }

    private void checkRegistrationUUID() {
        SharedPreferences sharedPref = this.getSharedPreferences(getString(R.string.eyeframe_prefs), Context.MODE_PRIVATE);
        if(!sharedPref.contains(getString(R.string.eyeframe_uuid))) {
            try {
                sendRequestToServer(sharedPref, UUID.randomUUID().toString(), this);
            } catch (JSONException e) {
            }
        }
    }

    private void sendRequestToServer(final SharedPreferences sharedPref, final String uuid, final Context ctx) throws JSONException {
        final JSONObject uuidJson = new JSONObject();
        uuidJson.put("uuid", uuid);
        final RequestQueue queue = Volley.newRequestQueue(this);
        final JsonObjectRequest jsr = new JsonObjectRequest(Request.Method.POST, URL + "/devices", uuidJson, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("status").equals("success")) {
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString(getString(R.string.eyeframe_uuid), uuid);
                        editor.commit();
                    }
                    Toast.makeText(ctx, response.getString("message"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(jsr);
    }

    private void setReceiverOnClickListener() {
        Button receiverBtn = (Button)findViewById(R.id.receiver_btn);
        receiverBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent receiverIntent = new Intent(MainActivity.this, ReceiverActivity.class);
                startActivity(receiverIntent);
            }
        });
    }
}
