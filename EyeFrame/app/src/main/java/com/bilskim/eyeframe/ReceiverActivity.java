package com.bilskim.eyeframe;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class ReceiverActivity extends AppCompatActivity {
    final Set<Target> pfgct = new HashSet<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receiver);
        TextView pinText = (TextView)findViewById(R.id.pinTextView);
        Button generatePinBtn = (Button)findViewById(R.id.generatePinBtn);
        Button buttonSetWallpaper = (Button)findViewById(R.id.receive);
        setGeneratePinOnClickListener(generatePinBtn, pinText);
        setReceiveWallpaperOnClickListener(buttonSetWallpaper);
    }

    private void setReceiveWallpaperOnClickListener(Button btn) {
        btn.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                SharedPreferences sharedPrefs = getSharedPreferences(getString(R.string.eyeframe_prefs), Context.MODE_PRIVATE);
                String uuid = sharedPrefs.getString(getString(R.string.eyeframe_uuid), "");
                String url = MainActivity.URL + "/devices/" + uuid + "/provide_wallpaper";
                BitmapTarget btmt = new BitmapTarget(getApplicationContext(), pfgct);
                Picasso.with(arg0.getContext()).load(url).into(btmt);
            }
        });
    }

    private void setGeneratePinOnClickListener(Button generatePinBtn, final TextView pinText) {
        generatePinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random random = new Random();
                try {
                    sendPinToServer(String.format("%08d", random.nextInt(100000000)), pinText, v.getContext());
                } catch (JSONException e) {
                }
            }
        });
    }

    private void sendPinToServer(final String pin, final TextView pinText, final Context ctx) throws JSONException {
        final JSONObject uuidJson = new JSONObject();
        SharedPreferences sharedPrefs = this.getSharedPreferences(getString(R.string.eyeframe_prefs), Context.MODE_PRIVATE);
        String uuid = sharedPrefs.getString(getString(R.string.eyeframe_uuid), "");
        uuidJson.put("pin", pin);
        final RequestQueue queue = Volley.newRequestQueue(this);
        final JsonObjectRequest jsr = new JsonObjectRequest(Request.Method.POST, MainActivity.URL + "/devices/" + uuid + "/create_pin", uuidJson, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("status").equals("success")) {
                        pinText.setText(pin);
                    }
                    Toast.makeText(ctx, response.getString("message"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(jsr);
    }
}
