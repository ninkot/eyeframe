package com.bilskim.eyeframe;

import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.IOException;
import java.util.Set;

/**
 * Created by bilskim on 02/06/2017.
 */

public class BitmapTarget implements Target {
    Set<Target> t;
    Context c;

    public BitmapTarget(Context c, Set<Target> t){
        this.c = c;
        this.t = t;
        this.t.add(this);
    }

    @Override
    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
        try {
            WindowManager windowManager = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
            Display display = windowManager.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;
            int height = size.y;
            Matrix matrix = new Matrix();

            matrix.postRotate(90);

            Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);

            Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap , 0, 0, scaledBitmap .getWidth(), scaledBitmap .getHeight(), matrix, true);
            WallpaperManager.getInstance(c).setBitmap(rotatedBitmap);
            Toast.makeText(this.c, "Wallper loaded", Toast.LENGTH_LONG).show();
            removeFromGarbage();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBitmapFailed(Drawable drawable) {
        removeFromGarbage();
    }

    @Override
    public void onPrepareLoad(Drawable drawable) {}

    private void removeFromGarbage(){
        t.remove(this);
    }
}
