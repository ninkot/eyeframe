package com.bilskim.eyeframe;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import static com.bilskim.eyeframe.R.id.pinText;

public class ProviderActivity extends AppCompatActivity {
    private String currentUUID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider);
        EditText pinText = (EditText) findViewById(R.id.pinText);
        Button sendPinBtn = (Button)findViewById(R.id.sendPinBtn);
        final ListView providersList = (ListView)findViewById(R.id.providerList);
        setSendPinBtnClickListener(sendPinBtn, pinText);
        try {
            indexReceivers(this, providersList);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        providersList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                JSONObject item = (JSONObject) providersList.getItemAtPosition(position);
                try {
                    currentUUID = item.getString("uuid");
                } catch (JSONException e) {
                }
                File file = new File(Environment.getExternalStorageDirectory(), "eyeframe/"+ currentUUID +".jpg");
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Uri outputFileUri = Uri.fromFile(file);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                startActivityForResult(intent, 1888);
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1888 && resultCode == Activity.RESULT_OK) {
            File file = new File(Environment.getExternalStorageDirectory(), "eyeframe/"+ currentUUID +".jpg");
            Uri outputFileUri = Uri.fromFile(file);
            sendBitmapToServer(outputFileUri.getPath(), currentUUID);

        }
    }

    private void sendBitmapToServer(String photo, String to_uuid) {
        SharedPreferences sharedPrefs = this.getSharedPreferences(getString(R.string.eyeframe_prefs), Context.MODE_PRIVATE);
        String uuid = sharedPrefs.getString(getString(R.string.eyeframe_uuid), "");
        final RequestQueue queue = Volley.newRequestQueue(this);
        String path = MainActivity.URL + "/devices/" + uuid + "/receive_wallpaper?to_uuid=" + to_uuid;

        MultipartRequest multipartRequest =

                new MultipartRequest(path, photo, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });
        queue.add(multipartRequest);
    }

    private void indexReceivers(final Activity activity, final ListView providersList) throws JSONException {
        SharedPreferences sharedPrefs = this.getSharedPreferences(getString(R.string.eyeframe_prefs), Context.MODE_PRIVATE);
        String uuid = sharedPrefs.getString(getString(R.string.eyeframe_uuid), "");
        final RequestQueue queue = Volley.newRequestQueue(this);
        final JsonArrayRequest jsr = new JsonArrayRequest(Request.Method.GET, MainActivity.URL + "/devices?uuid=" + uuid, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                providersList.setAdapter(new JSONAdapter(activity, response));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(jsr);
    }

    private void setSendPinBtnClickListener(Button sendPinBtn, final EditText pinText) {
        sendPinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pin = pinText.getText().toString();
                if(!pin.isEmpty()){
                    try {
                        pairDevices(pin, v.getContext());
                    } catch (JSONException e) {
                    }
                }
            }
        });
    }

    private void pairDevices(final String pin, final Context ctx) throws JSONException {
        final JSONObject uuidJson = new JSONObject();
        SharedPreferences sharedPrefs = this.getSharedPreferences(getString(R.string.eyeframe_prefs), Context.MODE_PRIVATE);
        String uuid = sharedPrefs.getString(getString(R.string.eyeframe_uuid), "");
        uuidJson.put("pin", pin);
        final RequestQueue queue = Volley.newRequestQueue(this);
        final JsonObjectRequest jsr = new JsonObjectRequest(Request.Method.POST, MainActivity.URL + "/devices/" + uuid + "/pair_devices", uuidJson, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Toast.makeText(ctx, response.getString("message"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(jsr);
    }
}
